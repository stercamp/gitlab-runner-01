import { Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';
const cdk = require('aws-cdk-lib');
import * as ec2 from "aws-cdk-lib/aws-ec2";
import * as iam from 'aws-cdk-lib/aws-iam';

import * as elbv2 from 'aws-cdk-lib/aws-elasticloadbalancingv2';

import * as autoscaling from 'aws-cdk-lib/aws-autoscaling'

export class CfmlEnvCdkAppStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const stack = cdk.Stack.of(this);
    const mydur = cdk.Duration.seconds(300);

    console.log('Region 👉 ', stack.region);
    //console.log('Qualifier 👉 ', stack.qualifier);

    // const theVPCID = new cdk.CfnParameter(this, 'theVPCID', {
    //     type: 'String',
    //     description: 'The VPC ID to deploy stacks',
    // });

    const importedVPCID = this.node.tryGetContext('pVPCID');
    console.log('importedVPCID 👉 ', importedVPCID);
    // console.log('theVPCID 👉 ', theVPCID.valueAsString);
    // console.log('vpcCidrBlock 👉 ', vpc.vpcCidrBlock);

    // Get Existing VPC
    // const vpc = ec2.Vpc.fromLookup(this, 'ImportVPC',{ vpcId: 'vpc-00dcec59e4753cbb5' });
    const vpc = ec2.Vpc.fromLookup(this, 'ImportVPC',{ vpcId: importedVPCID });
 

    console.log('vpcId 👉 ', vpc.vpcId);
    console.log('vpcCidrBlock 👉 ', vpc.vpcCidrBlock);

    const importedSubnet1ID = this.node.tryGetContext('pSubnet1ID');
    const importedSubnet1AZ = this.node.tryGetContext('pSubnet1AZ');
    const importedSubnet2ID = this.node.tryGetContext('pSubnet2ID');
    const importedSubnet2AZ = this.node.tryGetContext('pSubnet2AZ');

    console.log('imported Subnet1ID 👉 ', importedSubnet1ID);
    console.log('imported Subnet1AZ 👉 ', importedSubnet1AZ);
    console.log('imported Subnet2ID 👉 ', importedSubnet2ID);
    console.log('imported Subnet2AZ 👉 ', importedSubnet2AZ);


    // Get Existing Security Group for EC2 Instances
    const importedSecurityGroup = ec2.SecurityGroup.fromSecurityGroupId(this, 'SecurityGroup', cdk.Fn.importValue('EC2AccessSecurityGroupID'));
     
    // const importedRole = iam.Role.fromRoleArn(
    //   this,
    //   'imported-role',
    //   `arn:aws:iam::${cdk.Stack.of(this).account}:role/developer_bound/secrets-ec2-role-${cdk.Stack.of(this).account}-${cdk.Stack.of(this).region}`,
    //   {mutable: false},
    // );
    
    //console.log('SecretEC2IAMRoleName 👉', cdk.Fn.importValue('SecretEC2IAMRoleName'));


    // const importedRole = iam.Role.fromRoleArn(
    //     this,
    //     'imported-role',
    //     `arn:aws:iam::${cdk.Stack.of(this).account}:role/developer_bound/${cdk.Fn.importValue('SecretEC2IAMRoleName')}`
    // );

    // const importedRoleARN = this.node.tryGetContext('pEC2RoleARN');

    // console.log('importedRoleARN 👉 ', importedRoleARN);


    const importedPermissionBoundary = this.node.tryGetContext('pPermissionBoundary');

    const importedRole = iam.Role.fromRoleArn(
        this,
        'imported-role',
        `arn:aws:iam::${cdk.Stack.of(this).account}:role/${importedPermissionBoundary}/${cdk.Fn.importValue('SecretEC2IAMRoleName')}`,
        {mutable: false},
    );
    
    console.log('importedRole 👉', importedRole.roleName);
    // const ami = ec2.MachineImage.latestWindows(ec2.WindowsVersion.WINDOWS_SERVER_2019_ENGLISH_FULL_BASE);
    // const ami = ec2.MachineImage.genericWindows({
    //     'us-east-1': 'ami-033594f8862b03bb2'
    // });
    
    // const importedAMI = this.node.tryGetContext('pAMI');

    //     const ami = ec2.MachineImage.genericWindows({
    //     cdk.Stack.of(this).region: importedAMI
    // });

    // const region1 = stack.region;
    
    // const importedAMI = this.node.tryGetContext('pAMI');

    //     const ami = ec2.MachineImage.genericWindows({
    //         region1: importedAMI
    // });

    const importedAMI = this.node.tryGetContext('pAMI');

    const ami = ec2.MachineImage.genericWindows({
        [stack.region]: importedAMI
    });
    
    

    const subnet1 = ec2.Subnet.fromSubnetAttributes(this, 'private-subnet-1', {
        subnetId: importedSubnet1ID, //'subnet-09d63c5e56bc0d013',
        availabilityZone: importedSubnet1AZ //'us-east-1c'
    })

    const subnet2 = ec2.Subnet.fromSubnetAttributes(this, 'private-subnet-2', {
        subnetId: importedSubnet2ID, //'subnet-0b84aefbcc1678287',
        availabilityZone: importedSubnet2AZ //'us-east-1d'
    })

    const subnets = [subnet1, subnet2]


    const alb = new elbv2.ApplicationLoadBalancer(this, 'alb', {
      vpc,
      internetFacing: true,
      vpcSubnets: vpc.selectSubnets({ 
        subnets: subnets
       }),
    });
    // Create ALB Listener
    const listener = alb.addListener('Listener', {
        port: 80,
        open: true,
    });

    const asg = new autoscaling.AutoScalingGroup(this, 'asg', {
      vpc,
      instanceType: ec2.InstanceType.of(ec2.InstanceClass.R5, ec2.InstanceSize.LARGE),
      machineImage: ami,
      securityGroup: importedSecurityGroup,
      keyName: cdk.Fn.importValue('EC2KeyPairName'), //'ec2-key-pair',
      role: importedRole,
      vpcSubnets: vpc.selectSubnets({ 
        subnets: subnets
       }),
      //userData: initData,
      minCapacity: 2,
      maxCapacity: 3,
      init: ec2.CloudFormationInit.fromElements(ec2.InitFile.fromString("C:\\Program Files\\Amazon\\\AmazonCloudWatchAgent\\config.json", cdk.Fn.join("", [
          "{",
          "          \"logs\": {",
          "            \"logs_collected\": {",
          "              \"files\": {",
          "                \"collect_list\": [",
          "                 {",
          "                   \"file_path\": \"C:\\\\cfn\\\\log\\\\cfn-init.log\",",
          "                   \"log_group_name\": \"{instance_id}\",",
          "                   \"log_stream_name\": \"cfn-init-log\",",
          "                   \"timestamp_format\": \"%H: %M: %S%y%b%-d\"",
          "                 },",
          "                 {",
          "                   \"file_path\": \"C:\\\\cfn\\\\log\\\\cfn-init-cmd.log\",",
          "                   \"log_group_name\": \"{instance_id}\",",
          "                   \"log_stream_name\": \"cfn-init-log-cmd\",",
          "                   \"timestamp_format\": \"%H: %M: %S%y%b%-d\"",
          "                 }",
          "               ]",
          "             },",
          "              \"windows_events\": {",
          "                \"collect_list\": [",
          "                  {",
          "                    \"event_name\": \"System\",",
          "                    \"event_levels\": [",
          "                      \"INFORMATION\",",
          "                      \"ERROR\"",
          "                    ],",
          "                    \"log_group_name\": \"{instance_id}\",",
          "                    \"log_stream_name\": \"system\",",
          "                    \"event_format\": \"xml\"",
          "                  },",
          "                  {",
          "                    \"event_name\": \"Application\",",
          "                    \"event_levels\": [",
          "                      \"INFORMATION\",",
          "                      \"ERROR\"",
          "                    ],",
          "                    \"log_group_name\": \"{instance_id}\",",
          "                    \"log_stream_name\": \"application\",",
          "                    \"event_format\": \"xml\"",
          "                  }",
          "                ]",
          "              }",
          "           }",
          "          }",
          "        }"
        ])), ec2.InitPackage.msi("https://s3.amazonaws.com/amazoncloudwatch-agent/windows/amd64/latest/amazon-cloudwatch-agent.msi"), ec2.InitPackage.msi("https://s3.amazonaws.com/aws-cli/AWSCLI64.msi"), ec2.InitPackage.msi("https://github.com/aws/aws-sam-cli/releases/latest/download/AWS_SAM_CLI_64_PY3.msi"), ec2.InitFile.fromAsset("c:\\cfn\\tools\\gitBootstrapScript.ps1", "./resources/gitBootstrapScript.ps1"), ec2.InitFile.fromAsset("c:\\cfn\\tools\\appBootstrapScript.ps1", "./resources/appBootstrapScript.ps1"), ec2.InitCommand.shellCommand("powershell.exe -Command \"C:\\cfn\\tools\\appBootstrapScript.ps1\"", { key: "03-app-scripting", waitAfterCompletion: ec2.InitCommandWaitDuration.none() }), ec2.InitCommand.shellCommand("powershell.exe -Command \"C:\\cfn\\tools\\gitBootstrapScript.ps1\"", { key: "02-git-scripting", waitAfterCompletion: ec2.InitCommandWaitDuration.none() })),
        signals: autoscaling.Signals.waitForAll({
            timeout: cdk.Duration.minutes(15),
        }),
    });    

    // asg.applyRemovalPolicy(cdk.RemovalPolicy.RETAIN);

    listener.addTargets('default-target', {
        port: 80,
        targets: [asg],
        healthCheck: {
            path: '/',
            unhealthyThresholdCount: 2,
            healthyThresholdCount: 5,
            interval: cdk.Duration.seconds(30),
        },
    });
    listener.addAction('/static', {
        priority: 5,
        conditions: [elbv2.ListenerCondition.pathPatterns(['/static'])],
        action: elbv2.ListenerAction.fixedResponse(200, {
            contentType: 'text/html',
            messageBody: '<h1>Static ALB Response</h1>',
        }),
    });
    asg.scaleOnRequestCount('requests-per-minute', {
        targetRequestsPerMinute: 60,
    });
    asg.scaleOnCpuUtilization('cpu-util-scaling', {
        targetUtilizationPercent: 75,
    });

    new cdk.CfnOutput(this, 'albDNS', {
        value: alb.loadBalancerDnsName,
        exportName: "AppLoadBalanceRUL",
    });

  }
}
//exports.CfmlEnvCdkAppStack = CfmlEnvCdkAppStack;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2ZtbC1lbnYtc3RhY2suanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjZm1sLWVudi1zdGFjay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSxxQ0FBcUM7QUFDckMsMkNBQTJDO0FBQzNDLHdEQUF3RDtBQUN4RCx3Q0FBd0M7QUFDeEMsNkRBQTZEO0FBQzdELHdDQUF1QztBQUV2Qyx1REFBMkM7QUFNM0MsTUFBYSxZQUFhLFNBQVEsR0FBRyxDQUFDLEtBQUs7SUFDekMsWUFBWSxLQUFvQixFQUFFLEVBQVUsRUFBRSxLQUFzQjtRQUNsRSxLQUFLLENBQUMsS0FBSyxFQUFFLEVBQUUsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUV4Qiw2Q0FBNkM7UUFFN0MsbUJBQW1CO1FBQ25CLHdEQUF3RDtRQUN4RCxpREFBaUQ7UUFDakQsTUFBTTtRQUlOLHdCQUF3QjtRQUN4QixNQUFNLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsY0FBYyxFQUFFO1lBQ25ELFNBQVMsRUFBRSxJQUFJO1NBQ2hCLENBQUMsQ0FBQztRQUVILGlCQUFpQjtRQUNqQixPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsRUFBRSxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7UUFHbEQsYUFBYTtRQUNiLE1BQU0sR0FBRyxHQUFHLElBQUksS0FBSyxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxLQUFLLEVBQUU7WUFDekQsR0FBRztZQUNILGNBQWMsRUFBRSxJQUFJO1NBQ3JCLENBQUMsQ0FBQztRQUVILHNCQUFzQjtRQUN0QixNQUFNLFFBQVEsR0FBRyxHQUFHLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRTtZQUMzQyxJQUFJLEVBQUUsRUFBRTtZQUNSLElBQUksRUFBRSxJQUFJO1NBQ1gsQ0FBQyxDQUFDO1FBRUgsaURBQWlEO1FBQ2pELE1BQU0sYUFBYSxHQUFHLElBQUksR0FBRyxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsZUFBZSxFQUFFO1lBQ2pFLEdBQUc7WUFDSCxXQUFXLEVBQUUsOEJBQThCO1lBQzNDLGdCQUFnQixFQUFFLElBQUk7U0FDdkIsQ0FBQyxDQUFDO1FBQ0gsYUFBYSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLGtCQUFrQixDQUFDLENBQUE7UUFDdkcsYUFBYSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLGtCQUFrQixDQUFDLENBQUE7UUFDdkcsYUFBYSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLG1CQUFtQixDQUFDLENBQUE7UUFDdEcsYUFBYSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLG1CQUFtQixDQUFDLENBQUE7UUFFdEcsc0JBQXNCO1FBQ3RCLE1BQU0sSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsU0FBUyxFQUFFO1lBQ3pDLFNBQVMsRUFBRSxJQUFJLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQztTQUN6RCxDQUFDLENBQUE7UUFFRiwrQkFBK0I7UUFDL0IsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsd0JBQXdCLENBQUMsOEJBQThCLENBQUMsQ0FBQyxDQUFBO1FBRWpHLE1BQU0sR0FBRyxHQUFHLEdBQUcsQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMscUNBQXFDLENBQUMsQ0FBQztRQUVyRyw2Q0FBNkM7UUFFN0Msd0RBQXdEO1FBRXhELDBFQUEwRTtRQUMxRSwrR0FBK0c7UUFDL0cscUdBQXFHO1FBQ3JHLG9MQUFvTDtRQUNwTCxvT0FBb087UUFFcE8sSUFBSTtRQUVKLE1BQU0sR0FBRyxHQUFHLElBQUksMEJBQU8sQ0FBQyxJQUFJLEVBQUUsU0FBUyxFQUFFO1lBQ3ZDLElBQUksRUFBRSw4QkFBOEI7WUFDcEMsV0FBVyxFQUFFLHNDQUFzQztTQUNwRCxDQUFDLENBQUM7UUFDSCxHQUFHLENBQUMsb0JBQW9CLENBQUE7UUFFeEIsTUFBTSxHQUFHLEdBQUcsSUFBSSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRTtZQUN4RCxHQUFHO1lBQ0gsWUFBWSxFQUFFLEdBQUcsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUMvQixHQUFHLENBQUMsYUFBYSxDQUFDLEVBQUUsRUFDcEIsR0FBRyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQ3ZCO1lBQ0QsWUFBWSxFQUFFLEdBQUc7WUFDakIsYUFBYSxFQUFFLGFBQWE7WUFDNUIsT0FBTyxFQUFFLEdBQUcsQ0FBQyxXQUFXO1lBQ3hCLElBQUksRUFBRSxJQUFJO1lBQ1YscUJBQXFCO1lBQ3JCLFdBQVcsRUFBRSxDQUFDO1lBQ2QsV0FBVyxFQUFFLENBQUM7WUFDZCxJQUFJLEVBQUUsR0FBRyxDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FDdkMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsc0JBQXNCLEVBQUUsMENBQTBDLENBQUMsRUFDM0YsR0FBRyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsK0NBQStDLENBQUMsRUFDcEUsR0FBRyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsb0ZBQW9GLENBQUMsRUFDekcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsd0NBQXdDLEVBQUUsb0NBQW9DLENBQUMsRUFDdEcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsd0NBQXdDLEVBQUUsb0NBQW9DLENBQUMsRUFDdEcsR0FBRyxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsb0VBQW9FLEVBQUUsRUFBRSxHQUFHLEVBQUUsa0JBQWtCLEVBQUUsbUJBQW1CLEVBQUUsR0FBRyxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsRUFDeEwsR0FBRyxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsb0VBQW9FLEVBQUUsRUFBRSxHQUFHLEVBQUUsa0JBQWtCLEVBQUUsbUJBQW1CLEVBQUUsR0FBRyxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FDekw7WUFDRCxPQUFPLEVBQUUsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUM7Z0JBQ3RDLE9BQU8sRUFBRSxHQUFHLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7YUFDbEMsQ0FBQztTQUVILENBQUMsQ0FBQztRQUVILFFBQVEsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUU7WUFDcEMsSUFBSSxFQUFFLEVBQUU7WUFDUixPQUFPLEVBQUUsQ0FBQyxHQUFHLENBQUM7WUFDZCxXQUFXLEVBQUU7Z0JBQ1gsSUFBSSxFQUFFLEdBQUc7Z0JBQ1QsdUJBQXVCLEVBQUUsQ0FBQztnQkFDMUIscUJBQXFCLEVBQUUsQ0FBQztnQkFDeEIsUUFBUSxFQUFFLEdBQUcsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQzthQUNuQztTQUNGLENBQUMsQ0FBQztRQUVILFFBQVEsQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFO1lBQzVCLFFBQVEsRUFBRSxDQUFDO1lBQ1gsVUFBVSxFQUFFLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDL0QsTUFBTSxFQUFFLEtBQUssQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLEdBQUcsRUFBRTtnQkFDOUMsV0FBVyxFQUFFLFdBQVc7Z0JBQ3hCLFdBQVcsRUFBRSw4QkFBOEI7YUFDNUMsQ0FBQztTQUNILENBQUMsQ0FBQztRQUVILEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsRUFBRTtZQUM3Qyx1QkFBdUIsRUFBRSxFQUFFO1NBQzVCLENBQUMsQ0FBQztRQUVILEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxrQkFBa0IsRUFBRTtZQUM1Qyx3QkFBd0IsRUFBRSxFQUFFO1NBQzdCLENBQUMsQ0FBQztRQUVILElBQUksR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsUUFBUSxFQUFFO1lBQ2hDLEtBQUssRUFBRSxHQUFHLENBQUMsbUJBQW1CO1NBQy9CLENBQUMsQ0FBQztJQUlMLENBQUM7Q0FDRjtBQXpJRCxvQ0F5SUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBjZGsgZnJvbSAnQGF3cy1jZGsvY29yZSc7XG4vLyBpbXBvcnQgKiBhcyBzcXMgZnJvbSAnQGF3cy1jZGsvYXdzLXNxcyc7XG5pbXBvcnQgKiBhcyBhdXRvc2NhbGluZyBmcm9tICdAYXdzLWNkay9hd3MtYXV0b3NjYWxpbmcnO1xuaW1wb3J0ICogYXMgZWMyIGZyb20gJ0Bhd3MtY2RrL2F3cy1lYzInO1xuaW1wb3J0ICogYXMgZWxidjIgZnJvbSAnQGF3cy1jZGsvYXdzLWVsYXN0aWNsb2FkYmFsYW5jaW5ndjInO1xuaW1wb3J0ICogYXMgaWFtIGZyb20gJ0Bhd3MtY2RrL2F3cy1pYW0nXG5pbXBvcnQgKiBhcyBwYXRoIGZyb20gJ3BhdGgnO1xuaW1wb3J0IHsgS2V5UGFpciB9IGZyb20gJ2Nkay1lYzIta2V5LXBhaXInO1xuaW1wb3J0IHsgQXNzZXQgfSBmcm9tICdAYXdzLWNkay9hd3MtczMtYXNzZXRzJztcbmltcG9ydCB7IEluaXRDb21tYW5kIH0gZnJvbSBcIkBhd3MtY2RrL2F3cy1lYzJcIjtcbmltcG9ydCB7IER1cmF0aW9uIH0gZnJvbSAnQGF3cy1jZGsvY29yZSc7XG5cblxuZXhwb3J0IGNsYXNzIENmbWxFbnZTdGFjayBleHRlbmRzIGNkay5TdGFjayB7XG4gIGNvbnN0cnVjdG9yKHNjb3BlOiBjZGsuQ29uc3RydWN0LCBpZDogc3RyaW5nLCBwcm9wcz86IGNkay5TdGFja1Byb3BzKSB7XG4gICAgc3VwZXIoc2NvcGUsIGlkLCBwcm9wcyk7XG5cbiAgICAvLyBUaGUgY29kZSB0aGF0IGRlZmluZXMgeW91ciBzdGFjayBnb2VzIGhlcmVcblxuICAgIC8vIGV4YW1wbGUgcmVzb3VyY2VcbiAgICAvLyBjb25zdCBxdWV1ZSA9IG5ldyBzcXMuUXVldWUodGhpcywgJ0NmbWxFYzIwNFF1ZXVlJywge1xuICAgIC8vICAgdmlzaWJpbGl0eVRpbWVvdXQ6IGNkay5EdXJhdGlvbi5zZWNvbmRzKDMwMClcbiAgICAvLyB9KTtcblxuXG5cbiAgICAvLyDwn5GHIGltcG9ydCBWUEMgYnkgTmFtZVxuICAgIGNvbnN0IHZwYyA9IGVjMi5WcGMuZnJvbUxvb2t1cCh0aGlzLCAnZXh0ZXJuYWwtdnBjJywge1xuICAgICAgaXNEZWZhdWx0OiB0cnVlLFxuICAgIH0pO1xuICAgIFxuICAgIC8vU2hvdyBtZSB0aGUgVlBDXG4gICAgY29uc29sZS5sb2coJ3ZwY0lkIPCfkYkgJywgdnBjLnZwY0lkKTtcbiAgICBjb25zb2xlLmxvZygndnBjQ2lkckJsb2NrIPCfkYkgJywgdnBjLnZwY0NpZHJCbG9jayk7XG5cblxuICAgIC8vIENyZWF0ZSBBTEJcbiAgICBjb25zdCBhbGIgPSBuZXcgZWxidjIuQXBwbGljYXRpb25Mb2FkQmFsYW5jZXIodGhpcywgJ2FsYicsIHtcbiAgICAgIHZwYyxcbiAgICAgIGludGVybmV0RmFjaW5nOiB0cnVlLFxuICAgIH0pO1xuXG4gICAgLy8gQ3JlYXRlIEFMQiBMaXN0ZW5lclxuICAgIGNvbnN0IGxpc3RlbmVyID0gYWxiLmFkZExpc3RlbmVyKCdMaXN0ZW5lcicsIHtcbiAgICAgIHBvcnQ6IDgwLFxuICAgICAgb3BlbjogdHJ1ZSxcbiAgICB9KTtcblxuICAgIC8vIEFsbG93IFJEUCAoVENQIFBvcnQgMzM4OSkgYWNjZXNzIGZyb20gYW55d2hlcmVcbiAgICBjb25zdCBzZWN1cml0eUdyb3VwID0gbmV3IGVjMi5TZWN1cml0eUdyb3VwKHRoaXMsICdTZWN1cml0eUdyb3VwJywge1xuICAgICAgdnBjLFxuICAgICAgZGVzY3JpcHRpb246ICdBbGxvdyBSRFAgKFRDUCBwb3J0IDMzODkpIGluJyxcbiAgICAgIGFsbG93QWxsT3V0Ym91bmQ6IHRydWVcbiAgICB9KTtcbiAgICBzZWN1cml0eUdyb3VwLmFkZEluZ3Jlc3NSdWxlKGVjMi5QZWVyLmlwdjQoJzY3LjE2NC4zNS4xOTgvMzInKSwgZWMyLlBvcnQudGNwKDMzODkpLCAnQWxsb3cgUkRQIEFjY2VzcycpXG4gICAgc2VjdXJpdHlHcm91cC5hZGRJbmdyZXNzUnVsZShlYzIuUGVlci5pcHY0KCc3My40MS4xNzMuMTI2LzMyJyksIGVjMi5Qb3J0LnRjcCgzMzg5KSwgJ0FsbG93IFJEUCBBY2Nlc3MnKVxuICAgIHNlY3VyaXR5R3JvdXAuYWRkSW5ncmVzc1J1bGUoZWMyLlBlZXIuaXB2NCgnNjcuMTY0LjM1LjE5OC8zMicpLCBlYzIuUG9ydC50Y3AoODApLCAnQWxsb3cgSFRUUCBBY2Nlc3MnKVxuICAgIHNlY3VyaXR5R3JvdXAuYWRkSW5ncmVzc1J1bGUoZWMyLlBlZXIuaXB2NCgnNzMuNDEuMTczLjEyNi8zMicpLCBlYzIuUG9ydC50Y3AoODApLCAnQWxsb3cgSFRUUCBBY2Nlc3MnKVxuXG4gICAgLy8gQ3JlYXRlIEVDMiBJQU0gUm9sZVxuICAgIGNvbnN0IHJvbGUgPSBuZXcgaWFtLlJvbGUodGhpcywgJ2VjMlJvbGUnLCB7XG4gICAgICBhc3N1bWVkQnk6IG5ldyBpYW0uU2VydmljZVByaW5jaXBhbCgnZWMyLmFtYXpvbmF3cy5jb20nKVxuICAgIH0pXG5cbiAgICAvLyBBZGQgSUFNIFBvbGljaWVzIHRvIEVDMiBSb2xlXG4gICAgcm9sZS5hZGRNYW5hZ2VkUG9saWN5KGlhbS5NYW5hZ2VkUG9saWN5LmZyb21Bd3NNYW5hZ2VkUG9saWN5TmFtZSgnQW1hem9uU1NNTWFuYWdlZEluc3RhbmNlQ29yZScpKVxuXG4gICAgY29uc3QgYW1pID0gZWMyLk1hY2hpbmVJbWFnZS5sYXRlc3RXaW5kb3dzKGVjMi5XaW5kb3dzVmVyc2lvbi5XSU5ET1dTX1NFUlZFUl8yMDE5X0VOR0xJU0hfRlVMTF9CQVNFKTtcblxuICAgIC8vY29uc3QgdXNlckRhdGEgPSBlYzIuVXNlckRhdGEuRm9yV2luZG93cygpO1xuXG4gICAgLy8gY29uc3QgaW5pdERhdGEgPSBlYzIuQ2xvdWRGb3JtYXRpb25Jbml0LmZyb21FbGVtZW50cyhcblxuICAgIC8vICAgZWMyLkluaXRQYWNrYWdlLm1zaShcImh0dHBzOi8vczMuYW1hem9uYXdzLmNvbS9hd3MtY2xpL0FXU0NMSTY0Lm1zaVwiKSxcbiAgICAvLyAgIGVjMi5Jbml0UGFja2FnZS5tc2koXCJodHRwczovL2dpdGh1Yi5jb20vYXdzL2F3cy1zYW0tY2xpL3JlbGVhc2VzL2xhdGVzdC9kb3dubG9hZC9BV1NfU0FNX0NMSV82NF9QWTMubXNpXCIpLFxuICAgIC8vICAgZWMyLkluaXRGaWxlLmZyb21Bc3NldChcImM6XFxcXGNmblxcXFxhcHBCb290c3RyYXBTY3JpcHQucHMxXCIsIFwiLi9yZXNvdXJjZXMvYXBwQm9vdHN0cmFwU2NyaXB0LnBzMVwiKSxcbiAgICAvLyAgIGVjMi5Jbml0Q29tbWFuZC5zaGVsbENvbW1hbmQoXCJwb3dlcnNoZWxsLmV4ZSAtQ29tbWFuZCBcXFwiQzpcXFxcY2ZuXFxcXGdpdEJvb3RzdHJhcFNjcmlwdC5wczFcXFwiXCIsIHsga2V5OiBcImdpdCBzY3JpcHRpbmdcIiwgd2FpdEFmdGVyQ29tcGxldGlvbjogZWMyLkluaXRDb21tYW5kV2FpdER1cmF0aW9uLm5vbmUoKSB9KSxcbiAgICAvLyAgIGVjMi5Jbml0Q29tbWFuZC5zaGVsbENvbW1hbmQoJ2Nmbi1zaWduYWwuZXhlIC1lICVFUlJPUkxFVkVMJSAtLXJlc291cmNlIEluc3RhbmNlQzEwNjNBODcgLS1zdGFjayAnICsgdGhpcy5zdGFja0lkICsgJyAtLXJlZ2lvbiAnICsgdGhpcy5yZWdpb24sIHsga2V5OiBcIkNGTi1TaWduYWxcIiwgd2FpdEFmdGVyQ29tcGxldGlvbjogZWMyLkluaXRDb21tYW5kV2FpdER1cmF0aW9uLm5vbmUoKSB9KVxuXG4gICAgLy8gKVxuXG4gICAgY29uc3Qga2V5ID0gbmV3IEtleVBhaXIodGhpcywgJ0tleVBhaXInLCB7XG4gICAgICBuYW1lOiAnY2RrLWNmbWwtZWMyLWFzZy1rZXlwYWlyLXdpbicsXG4gICAgICBkZXNjcmlwdGlvbjogJ0tleSBQYWlyIGNyZWF0ZWQgd2l0aCBDREsgRGVwbG95bWVudCcsXG4gICAgfSk7XG4gICAga2V5LmdyYW50UmVhZE9uUHVibGljS2V5XG5cbiAgICBjb25zdCBhc2cgPSBuZXcgYXV0b3NjYWxpbmcuQXV0b1NjYWxpbmdHcm91cCh0aGlzLCAnYXNnJywge1xuICAgICAgdnBjLFxuICAgICAgaW5zdGFuY2VUeXBlOiBlYzIuSW5zdGFuY2VUeXBlLm9mKFxuICAgICAgICBlYzIuSW5zdGFuY2VDbGFzcy5SNSxcbiAgICAgICAgZWMyLkluc3RhbmNlU2l6ZS5MQVJHRSxcbiAgICAgICksXG4gICAgICBtYWNoaW5lSW1hZ2U6IGFtaSxcbiAgICAgIHNlY3VyaXR5R3JvdXA6IHNlY3VyaXR5R3JvdXAsXG4gICAgICBrZXlOYW1lOiBrZXkua2V5UGFpck5hbWUsXG4gICAgICByb2xlOiByb2xlLFxuICAgICAgLy91c2VyRGF0YTogaW5pdERhdGEsXG4gICAgICBtaW5DYXBhY2l0eTogMixcbiAgICAgIG1heENhcGFjaXR5OiAzLFxuICAgICAgaW5pdDogZWMyLkNsb3VkRm9ybWF0aW9uSW5pdC5mcm9tRWxlbWVudHMoXG4gICAgICAgIGVjMi5Jbml0RmlsZS5mcm9tU3RyaW5nKFwiYzpcXFxcY2ZuXFxcXGNmbi1odXAudHh0XCIsIFwiVGhpcyBnb3Qgd3JpdHRlbiBkdXJpbmcgaW5zdGFuY2Ugc3RhcnR1cFwiKSxcbiAgICAgICAgZWMyLkluaXRQYWNrYWdlLm1zaShcImh0dHBzOi8vczMuYW1hem9uYXdzLmNvbS9hd3MtY2xpL0FXU0NMSTY0Lm1zaVwiKSxcbiAgICAgICAgZWMyLkluaXRQYWNrYWdlLm1zaShcImh0dHBzOi8vZ2l0aHViLmNvbS9hd3MvYXdzLXNhbS1jbGkvcmVsZWFzZXMvbGF0ZXN0L2Rvd25sb2FkL0FXU19TQU1fQ0xJXzY0X1BZMy5tc2lcIiksXG4gICAgICAgIGVjMi5Jbml0RmlsZS5mcm9tQXNzZXQoXCJjOlxcXFxjZm5cXFxcdG9vbHNcXFxcZ2l0Qm9vdHN0cmFwU2NyaXB0LnBzMVwiLCBcIi4vcmVzb3VyY2VzL2dpdEJvb3RzdHJhcFNjcmlwdC5wczFcIiksXG4gICAgICAgIGVjMi5Jbml0RmlsZS5mcm9tQXNzZXQoXCJjOlxcXFxjZm5cXFxcdG9vbHNcXFxcYXBwQm9vdHN0cmFwU2NyaXB0LnBzMVwiLCBcIi4vcmVzb3VyY2VzL2FwcEJvb3RzdHJhcFNjcmlwdC5wczFcIiksXG4gICAgICAgIGVjMi5Jbml0Q29tbWFuZC5zaGVsbENvbW1hbmQoXCJwb3dlcnNoZWxsLmV4ZSAtQ29tbWFuZCBcXFwiQzpcXFxcY2ZuXFxcXHRvb2xzXFxcXGFwcEJvb3RzdHJhcFNjcmlwdC5wczFcXFwiXCIsIHsga2V5OiBcIjAzLWFwcC1zY3JpcHRpbmdcIiwgd2FpdEFmdGVyQ29tcGxldGlvbjogZWMyLkluaXRDb21tYW5kV2FpdER1cmF0aW9uLm5vbmUoKSB9KSxcbiAgICAgICAgZWMyLkluaXRDb21tYW5kLnNoZWxsQ29tbWFuZChcInBvd2Vyc2hlbGwuZXhlIC1Db21tYW5kIFxcXCJDOlxcXFxjZm5cXFxcdG9vbHNcXFxcZ2l0Qm9vdHN0cmFwU2NyaXB0LnBzMVxcXCJcIiwgeyBrZXk6IFwiMDItZ2l0LXNjcmlwdGluZ1wiLCB3YWl0QWZ0ZXJDb21wbGV0aW9uOiBlYzIuSW5pdENvbW1hbmRXYWl0RHVyYXRpb24ubm9uZSgpIH0pLFxuICAgICAgKSxcbiAgICAgIHNpZ25hbHM6IGF1dG9zY2FsaW5nLlNpZ25hbHMud2FpdEZvckFsbCh7XG4gICAgICAgIHRpbWVvdXQ6IGNkay5EdXJhdGlvbi5taW51dGVzKDE1KSxcbiAgICAgIH0pLFxuICAgIFxuICAgIH0pO1xuXG4gICAgbGlzdGVuZXIuYWRkVGFyZ2V0cygnZGVmYXVsdC10YXJnZXQnLCB7XG4gICAgICBwb3J0OiA4MCxcbiAgICAgIHRhcmdldHM6IFthc2ddLFxuICAgICAgaGVhbHRoQ2hlY2s6IHtcbiAgICAgICAgcGF0aDogJy8nLFxuICAgICAgICB1bmhlYWx0aHlUaHJlc2hvbGRDb3VudDogMixcbiAgICAgICAgaGVhbHRoeVRocmVzaG9sZENvdW50OiA1LFxuICAgICAgICBpbnRlcnZhbDogY2RrLkR1cmF0aW9uLnNlY29uZHMoMzApLFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIGxpc3RlbmVyLmFkZEFjdGlvbignL3N0YXRpYycsIHtcbiAgICAgIHByaW9yaXR5OiA1LFxuICAgICAgY29uZGl0aW9uczogW2VsYnYyLkxpc3RlbmVyQ29uZGl0aW9uLnBhdGhQYXR0ZXJucyhbJy9zdGF0aWMnXSldLFxuICAgICAgYWN0aW9uOiBlbGJ2Mi5MaXN0ZW5lckFjdGlvbi5maXhlZFJlc3BvbnNlKDIwMCwge1xuICAgICAgICBjb250ZW50VHlwZTogJ3RleHQvaHRtbCcsXG4gICAgICAgIG1lc3NhZ2VCb2R5OiAnPGgxPlN0YXRpYyBBTEIgUmVzcG9uc2U8L2gxPicsXG4gICAgICB9KSxcbiAgICB9KTtcblxuICAgIGFzZy5zY2FsZU9uUmVxdWVzdENvdW50KCdyZXF1ZXN0cy1wZXItbWludXRlJywge1xuICAgICAgdGFyZ2V0UmVxdWVzdHNQZXJNaW51dGU6IDYwLFxuICAgIH0pO1xuXG4gICAgYXNnLnNjYWxlT25DcHVVdGlsaXphdGlvbignY3B1LXV0aWwtc2NhbGluZycsIHtcbiAgICAgIHRhcmdldFV0aWxpemF0aW9uUGVyY2VudDogNzUsXG4gICAgfSk7XG5cbiAgICBuZXcgY2RrLkNmbk91dHB1dCh0aGlzLCAnYWxiRE5TJywge1xuICAgICAgdmFsdWU6IGFsYi5sb2FkQmFsYW5jZXJEbnNOYW1lLFxuICAgIH0pO1xuXG5cblxuICB9XG59XG4iXX0=
