Configuration ConfigureGitLabRunnerEC2{
    param
    (
        [string] $ComputerName
    )

    try {
        Write-Host ($ComputerName)    
        $StartString = "Starting GitLab Runner EC2 configuration"
        Write-Host ($StartString)

        mkdir c:\gitlab-runner

        #GIT --Install
        wget https://github.com/git-for-windows/git/releases/download/v2.33.1.windows.1/Git-2.33.1-64-bit.exe -o c:\gitlab-runner\Git-2.33.1-64-bit.exe

        C:\gitlab-runner\Git-2.33.1-64-bit /SILENT

        Write-Host ("Successfully Installed Git")

        #NVM --Setup
        wget https://github.com/coreybutler/nvm-windows/releases/download/1.1.8/nvm-setup.zip -o c:\gitlab-runner\nvm-setup.zip

        Expand-Archive C:\gitlab-runner\nvm-setup.zip -DestinationPath C:\gitlab-runner

        C:\gitlab-runner\nvm-setup.exe /SILENT

        Write-Output ("Successfully Setup NVM")

        #GITLAB Runner --Install
        wget https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe -o C:\gitlab-runner\gitlab-runner.exe

        C:\gitlab-runner\gitlab-runner.exe install

        C:\gitlab-runner\gitlab-runner.exe start

        Write-Host ("Successfully Installed GitLab Runner")

        
    }
    catch {
        Write-Host ("Error Creating GitLab Runner")
        Write-Host $_
    }
    finally {
        write-Host ("Successfully configured GitLab Runner EC2");
    }
}


ConfigureGitLabRunnerEC2 -ComputerName $env:ComputerName