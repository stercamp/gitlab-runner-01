
Configuration ConfigureIISWebAppEC2{
    param(
        [string] $ComputerName
    )

    try{
        Write-Host ($ComputerName)    
        $SecureString = "Sterling is Cool"
        Write-Host ($SecureString)



        Import-Module WebAdministration
        Import-Module posh-git 

        $env:Path += ";C:\Program Files\Git\bin"

        Write-Host ("Successfully Imported Web and Git PS Modules")

        cd 'C:\inetpub\wwwroot'

        git clone 'https://gitlab.com/stercamp/gitlab-cfml-app-02.git'

        Set-ItemProperty IIS:\Sites\'Default Web Site' -name physicalPath -value "C:\inetpub\wwwroot\gitlab-cfml-app-02\public"

        Write-Host ("Successfully Configure Website and Git Repo Code")

    }
    catch {
        Write-Host ("Error Configuring IIS Web App EC2")
        $ErrorMessage = $_.Exception.Message;
        write-Host ($ErrorMessage);
    }
    finally{
        write-Host ("Successfully Configuring IIS Web App EC2");
    }
}


ConfigureIISWebAppEC2 -ComputerName $env:ComputerName