Configuration InstallGITEC2{
    param(
        [string] $ComputerName
    )

    try{
        Write-Host ($ComputerName)    
        $SecureString = "Sterling is Cool"
        Write-Host ($SecureString)

        
        $Folder = 'c:\cfn\tools'
        if (Test-Path -Path $Folder) {
            Write-Host("Path exists!")
        } else {
            Write-Host("Path doesn't exist")
            mkdir $Folder
        }

        #GIT --Install
        wget https://github.com/git-for-windows/git/releases/download/v2.33.1.windows.1/Git-2.33.1-64-bit.exe -o c:\cfn\tools\Git-2.33.1-64-bit.exe

        c:\cfn\tools\Git-2.33.1-64-bit /SILENT

        write-Host ("Successfully Installed Git")

        #NVM --Install
        wget https://github.com/coreybutler/nvm-windows/releases/download/1.1.8/nvm-setup.zip -o c:\cfn\tools\nvm-setup.zip

        Expand-Archive c:\cfn\tools\nvm-setup.zip -DestinationPath c:\cfn\tools

        c:\cfn\tools\nvm-setup.exe /SILENT

        write-Host ("Successfully Installed NVM")

        Add-WindowsFeature -Name Web-Server -IncludeManagementTools -LogPath c:\cfn\log\FeatureInstall.txt
        # Install-WindowsFeature -Name Web-Server -IncludeAllSubFeature -IncludeManagementTools

        write-Host ("Successfully Installed Web-Server")

        Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force

        write-Host ("Successfully Installed NuGet")

        Install-Module posh-git -Scope AllUsers -Force -SkipPublisherCheck

        write-Host ("Successfully Installed posh-git")

        


        
    }
    catch {
        #Write-Output ("Error Creating CFML EC2")
        $ErrorMessage = $_.Exception.Message;
        write-Host ($ErrorMessage);
    }
    finally{
        write-Host ("Successfully Installed GIT EC2");
    }
    
}


InstallGITEC2 -ComputerName $env:ComputerName