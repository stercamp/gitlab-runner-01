Configuration InstallNodeEC2{
    param
    (
        [string] $ComputerName
    )

    try {
        Write-Host ($ComputerName)    
        $StartString = "Starting Node configuration"
        Write-Host ($StartString)

        #Node --Install
        C:\Users\Administrator\AppData\Roaming\nvm\nvm.exe install 16.3.0

        Write-Output ("Successfully Installed Node")

        #Node --Install
        C:\Users\Administrator\AppData\Roaming\nvm\nvm.exe use 16.3.0       
      
        Write-Output ("Successfully Configured Node Version")

    }
    catch {
        Write-Output ("Error Installing Node")
        Write-Output $_
    }
    finally {
        write-Host ("Successfully Installed Node");
    }
}


InstallNodeEC2 -ComputerName $env:ComputerName