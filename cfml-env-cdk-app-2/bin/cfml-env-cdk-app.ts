#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { DefaultStackSynthesizer } from 'aws-cdk-lib';
import { CfmlEnvCdkAppStack } from '../lib/cfml-env-cdk-app-stack';
// const envDEV = { 
//     account: '880895181392', 
//     region: 'us-east-1'
// };
const app = new cdk.App();
new CfmlEnvCdkAppStack(app, 'CfmlEnvCdkAppStack', {
  env: { 
        account: app.node.tryGetContext('pAccountID'), //'880895181392', 
        region: app.node.tryGetContext('pRegion'), //'us-east-1'
    },
  synthesizer: new DefaultStackSynthesizer({
    
    qualifier: app.node.tryGetContext('pQualifier'), //'cfmlapp01', 
    deployRoleArn: 'arn:${AWS::Partition}:iam::${AWS::AccountId}:role/developer_bound/cdk-${Qualifier}-deploy-role-${AWS::AccountId}-${AWS::Region}',
    fileAssetPublishingRoleArn: 'arn:${AWS::Partition}:iam::${AWS::AccountId}:role/developer_bound/cdk-${Qualifier}-file-publishing-role-${AWS::AccountId}-${AWS::Region}',
    cloudFormationExecutionRole: 'arn:${AWS::Partition}:iam::${AWS::AccountId}:role/developer_bound/cdk-${Qualifier}-cfn-exec-role-${AWS::AccountId}-${AWS::Region}',
    lookupRoleArn: 'arn:${AWS::Partition}:iam::${AWS::AccountId}:role/developer_bound/cdk-${Qualifier}-lookup-role-${AWS::AccountId}-${AWS::Region}',
  }),

});
